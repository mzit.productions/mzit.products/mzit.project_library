﻿using System;
using System.Windows.Forms;


namespace Project_Library
{
    public partial class InsertPerson : Form
    {
        public InsertPerson()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Database db = new Database();
            db.Insert_Person(textBox1.Text, textBox2.Text, textBox3.Text);

            MessageBox.Show("ثبت کاربر با موفقیت انجام شد");
        }

        private void InsertPerson_Load(object sender, EventArgs e)
        {

        }
    }
}
