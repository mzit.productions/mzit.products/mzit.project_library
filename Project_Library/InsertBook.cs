﻿using System;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class InsertBook : Form
    {
        public InsertBook()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Database db = new Database();


            if (db.Insert_Book(txt_name.Text, txt_ather.Text, txt_publish.Text, txt_subject.Text))
            {
                MessageBox.Show("ثبت کتاب با موفقیت انجام شد");
            }
            else
            {
                MessageBox.Show("ثبت کتاب انجام نشد");
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
