﻿namespace Project_Library
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.مدیریتکاربرانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ثبتکاربرجدیدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.حذفکاربرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.جستجوکاربرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مدیریتاعضاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ثبتعضویتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.حذفوویرایشعضویتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.حذفعضویتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.جستجویاعضاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مدیریتکتابToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ثبتکتابToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.حذفوویراشکتابToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.جستجویکتابToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اماناتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ثبتاماناتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.کتابهایامانتدادهشدهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.خروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.label5 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.wmp_1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_1)).BeginInit();
            this.SuspendLayout();
            // 
            // مدیریتکاربرانToolStripMenuItem
            // 
            this.مدیریتکاربرانToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ثبتکاربرجدیدToolStripMenuItem,
            this.toolStripSeparator1,
            this.حذفکاربرToolStripMenuItem,
            this.toolStripSeparator2,
            this.جستجوکاربرToolStripMenuItem});
            this.مدیریتکاربرانToolStripMenuItem.Image = global::Project_Library.Properties.Resources.Leopard_Icon__101_;
            this.مدیریتکاربرانToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.مدیریتکاربرانToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.مدیریتکاربرانToolStripMenuItem.Name = "مدیریتکاربرانToolStripMenuItem";
            this.مدیریتکاربرانToolStripMenuItem.Size = new System.Drawing.Size(186, 46);
            this.مدیریتکاربرانToolStripMenuItem.Text = "مدیریت کاربران";
            // 
            // ثبتکاربرجدیدToolStripMenuItem
            // 
            this.ثبتکاربرجدیدToolStripMenuItem.Name = "ثبتکاربرجدیدToolStripMenuItem";
            this.ثبتکاربرجدیدToolStripMenuItem.Size = new System.Drawing.Size(236, 46);
            this.ثبتکاربرجدیدToolStripMenuItem.Text = "ثبت کاربر جدید";
            this.ثبتکاربرجدیدToolStripMenuItem.Click += new System.EventHandler(this.ثبتکاربرجدیدToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(233, 6);
            // 
            // حذفکاربرToolStripMenuItem
            // 
            this.حذفکاربرToolStripMenuItem.Name = "حذفکاربرToolStripMenuItem";
            this.حذفکاربرToolStripMenuItem.Size = new System.Drawing.Size(236, 46);
            this.حذفکاربرToolStripMenuItem.Text = "حذف کاربر";
            this.حذفکاربرToolStripMenuItem.Click += new System.EventHandler(this.حذفکاربرToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(233, 6);
            // 
            // جستجوکاربرToolStripMenuItem
            // 
            this.جستجوکاربرToolStripMenuItem.Name = "جستجوکاربرToolStripMenuItem";
            this.جستجوکاربرToolStripMenuItem.Size = new System.Drawing.Size(236, 46);
            this.جستجوکاربرToolStripMenuItem.Text = "جستجو کاربر";
            this.جستجوکاربرToolStripMenuItem.Click += new System.EventHandler(this.جستجوکاربرToolStripMenuItem_Click);
            // 
            // مدیریتاعضاToolStripMenuItem
            // 
            this.مدیریتاعضاToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ثبتعضویتToolStripMenuItem,
            this.toolStripSeparator3,
            this.حذفوویرایشعضویتToolStripMenuItem,
            this.toolStripSeparator4,
            this.حذفعضویتToolStripMenuItem,
            this.toolStripSeparator8,
            this.جستجویاعضاToolStripMenuItem});
            this.مدیریتاعضاToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("مدیریتاعضاToolStripMenuItem.Image")));
            this.مدیریتاعضاToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.مدیریتاعضاToolStripMenuItem.Name = "مدیریتاعضاToolStripMenuItem";
            this.مدیریتاعضاToolStripMenuItem.Size = new System.Drawing.Size(179, 46);
            this.مدیریتاعضاToolStripMenuItem.Text = "مدیریت اعضا";
            // 
            // ثبتعضویتToolStripMenuItem
            // 
            this.ثبتعضویتToolStripMenuItem.Name = "ثبتعضویتToolStripMenuItem";
            this.ثبتعضویتToolStripMenuItem.Size = new System.Drawing.Size(299, 46);
            this.ثبتعضویتToolStripMenuItem.Text = "ثبت عضویت";
            this.ثبتعضویتToolStripMenuItem.Click += new System.EventHandler(this.ثبتعضویتToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(296, 6);
            // 
            // حذفوویرایشعضویتToolStripMenuItem
            // 
            this.حذفوویرایشعضویتToolStripMenuItem.Name = "حذفوویرایشعضویتToolStripMenuItem";
            this.حذفوویرایشعضویتToolStripMenuItem.Size = new System.Drawing.Size(299, 46);
            this.حذفوویرایشعضویتToolStripMenuItem.Text = "ویرایش و حذف عضویت";
            this.حذفوویرایشعضویتToolStripMenuItem.Click += new System.EventHandler(this.حذفوویرایشعضویتToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(296, 6);
            // 
            // حذفعضویتToolStripMenuItem
            // 
            this.حذفعضویتToolStripMenuItem.Name = "حذفعضویتToolStripMenuItem";
            this.حذفعضویتToolStripMenuItem.Size = new System.Drawing.Size(299, 46);
            this.حذفعضویتToolStripMenuItem.Text = "حذف عضویت";
            this.حذفعضویتToolStripMenuItem.Visible = false;
            this.حذفعضویتToolStripMenuItem.Click += new System.EventHandler(this.حذفعضویتToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(296, 6);
            this.toolStripSeparator8.Visible = false;
            // 
            // جستجویاعضاToolStripMenuItem
            // 
            this.جستجویاعضاToolStripMenuItem.Name = "جستجویاعضاToolStripMenuItem";
            this.جستجویاعضاToolStripMenuItem.Size = new System.Drawing.Size(299, 46);
            this.جستجویاعضاToolStripMenuItem.Text = "جستجوی اعضا";
            this.جستجویاعضاToolStripMenuItem.Click += new System.EventHandler(this.جستجویاعضاToolStripMenuItem_Click);
            // 
            // مدیریتکتابToolStripMenuItem
            // 
            this.مدیریتکتابToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ثبتکتابToolStripMenuItem,
            this.toolStripSeparator5,
            this.حذفوویراشکتابToolStripMenuItem,
            this.toolStripSeparator6,
            this.جستجویکتابToolStripMenuItem});
            this.مدیریتکتابToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("مدیریتکتابToolStripMenuItem.Image")));
            this.مدیریتکتابToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.مدیریتکتابToolStripMenuItem.Name = "مدیریتکتابToolStripMenuItem";
            this.مدیریتکتابToolStripMenuItem.Size = new System.Drawing.Size(175, 46);
            this.مدیریتکتابToolStripMenuItem.Text = "مدیریت کتاب";
            // 
            // ثبتکتابToolStripMenuItem
            // 
            this.ثبتکتابToolStripMenuItem.Name = "ثبتکتابToolStripMenuItem";
            this.ثبتکتابToolStripMenuItem.Size = new System.Drawing.Size(271, 46);
            this.ثبتکتابToolStripMenuItem.Text = "ثبت کتاب";
            this.ثبتکتابToolStripMenuItem.Click += new System.EventHandler(this.ثبتکتابToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(268, 6);
            // 
            // حذفوویراشکتابToolStripMenuItem
            // 
            this.حذفوویراشکتابToolStripMenuItem.Name = "حذفوویراشکتابToolStripMenuItem";
            this.حذفوویراشکتابToolStripMenuItem.Size = new System.Drawing.Size(271, 46);
            this.حذفوویراشکتابToolStripMenuItem.Text = "ویراش و حذف کتاب";
            this.حذفوویراشکتابToolStripMenuItem.Click += new System.EventHandler(this.حذفوویراشکتابToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(268, 6);
            // 
            // جستجویکتابToolStripMenuItem
            // 
            this.جستجویکتابToolStripMenuItem.Name = "جستجویکتابToolStripMenuItem";
            this.جستجویکتابToolStripMenuItem.Size = new System.Drawing.Size(271, 46);
            this.جستجویکتابToolStripMenuItem.Text = "جستجوی کتاب";
            this.جستجویکتابToolStripMenuItem.Click += new System.EventHandler(this.جستجویکتابToolStripMenuItem_Click);
            // 
            // اماناتToolStripMenuItem
            // 
            this.اماناتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ثبتاماناتToolStripMenuItem,
            this.toolStripSeparator7,
            this.کتابهایامانتدادهشدهToolStripMenuItem});
            this.اماناتToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("اماناتToolStripMenuItem.Image")));
            this.اماناتToolStripMenuItem.Name = "اماناتToolStripMenuItem";
            this.اماناتToolStripMenuItem.Size = new System.Drawing.Size(103, 46);
            this.اماناتToolStripMenuItem.Text = "امانات";
            // 
            // ثبتاماناتToolStripMenuItem
            // 
            this.ثبتاماناتToolStripMenuItem.Name = "ثبتاماناتToolStripMenuItem";
            this.ثبتاماناتToolStripMenuItem.Size = new System.Drawing.Size(320, 46);
            this.ثبتاماناتToolStripMenuItem.Text = "ثبت امانات";
            this.ثبتاماناتToolStripMenuItem.Click += new System.EventHandler(this.ثبتاماناتToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(317, 6);
            // 
            // کتابهایامانتدادهشدهToolStripMenuItem
            // 
            this.کتابهایامانتدادهشدهToolStripMenuItem.Name = "کتابهایامانتدادهشدهToolStripMenuItem";
            this.کتابهایامانتدادهشدهToolStripMenuItem.Size = new System.Drawing.Size(320, 46);
            this.کتابهایامانتدادهشدهToolStripMenuItem.Text = "کتاب های امانت داده شده";
            this.کتابهایامانتدادهشدهToolStripMenuItem.Click += new System.EventHandler(this.کتابهایامانتدادهشدهToolStripMenuItem_Click);
            // 
            // خروجToolStripMenuItem
            // 
            this.خروجToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("خروجToolStripMenuItem.Image")));
            this.خروجToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.خروجToolStripMenuItem.Name = "خروجToolStripMenuItem";
            this.خروجToolStripMenuItem.Size = new System.Drawing.Size(108, 46);
            this.خروجToolStripMenuItem.Text = "خروج";
            this.خروجToolStripMenuItem.Click += new System.EventHandler(this.خروجToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("B Titr", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.مدیریتکاربرانToolStripMenuItem,
            this.مدیریتاعضاToolStripMenuItem,
            this.مدیریتکتابToolStripMenuItem,
            this.اماناتToolStripMenuItem,
            this.خروجToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(15, 5, 0, 5);
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(927, 56);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(191, 65);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 42);
            this.label5.TabIndex = 21;
            this.label5.Text = "label5";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 42);
            this.label1.TabIndex = 22;
            this.label1.Text = "lable4";
            // 
            // wmp_1
            // 
            this.wmp_1.Enabled = true;
            this.wmp_1.Location = new System.Drawing.Point(0, 329);
            this.wmp_1.Name = "wmp_1";
            this.wmp_1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmp_1.OcxState")));
            this.wmp_1.Size = new System.Drawing.Size(1163, 45);
            this.wmp_1.TabIndex = 23;
            // 
            // frmmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 42F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Project_Library.Properties.Resources._64785_155;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(927, 375);
            this.Controls.Add(this.wmp_1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("B Titr", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(8);
            this.Name = "frmmain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "برنامه کتابخانه";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem مدیریتکاربرانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ثبتکاربرجدیدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جستجوکاربرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مدیریتاعضاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ثبتعضویتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جستجویاعضاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مدیریتکتابToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ثبتکتابToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جستجویکتابToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اماناتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ثبتاماناتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem خروجToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Timer timer1;
        private AxWMPLib.AxWindowsMediaPlayer wmp_1;
        private System.Windows.Forms.ToolStripMenuItem حذفکاربرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حذفعضویتToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem حذفوویرایشعضویتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حذفوویراشکتابToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem کتابهایامانتدادهشدهToolStripMenuItem;
    }
}