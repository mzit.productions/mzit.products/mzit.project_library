﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class EditPerson : Form
    {
        public EditPerson()
        {
            InitializeComponent();
        }

        private void Edit_person_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'tbl_personDataSet.tbl_person' table. You can move, or remove it, as needed.
            tbl_personTableAdapter.Fill(tbl_personDataSet.tbl_person);
            // TODO: This line of code loads data into the 'tbl_userDataSet.tbl_user' table. You can move, or remove it, as needed.
            tbl_userTableAdapter.Fill(tbl_userDataSet.tbl_user);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tbl_book set name='" + textBox2.Text + "' , author='" + textBox3.Text + "' , publish = '" + textBox4.Text + "' , subject = '" + textBox5.Text + "' where id_book = '" + id + "'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            MessageBox.Show("اطلاعات با موفقیت ویرایش گردید");

            SqlConnection con1 = new SqlConnection(Database.ConnectionString);
            SqlCommand cmd1 = new SqlCommand("select * from tbl_person where name like '%" + textBox2.Text + "%'")
            {
                Connection = con1
            };
            DataSet ds1 = new DataSet();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            da1.Fill(ds1);
            dataGridView1.DataSource = ds1.Tables[0];
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand("delete from tbl_person where id_person = '" + id + "'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            MessageBox.Show("اطلاعات با موفقیت حذف گردید");
            dataGridView1.DataSource = null;
        }

        private void TextBox6_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            SqlCommand cmd = new SqlCommand("select * from tbl_person where name like N'%" + textBox6.Text + "%'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
        }
    }
}
