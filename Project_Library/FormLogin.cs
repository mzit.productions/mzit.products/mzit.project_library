﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Database db = new Database();
            if (db.Login(textBox1.Text, textBox2.Text))
            {
                Hide();
                FormMain f = new FormMain();
                f.ShowDialog();
            }
            else
                MessageBox.Show("رمز عبور یا نام کاربری نامعتبر است");
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            PersianCalendar pc = new PersianCalendar();
            label4.Text = pc.GetHour(DateTime.Now).ToString() + ":" + pc.GetMinute(DateTime.Now).ToString() + ":" + pc.GetSecond(DateTime.Now).ToString();
        }

        private void Form_login_Load(object sender, EventArgs e)
        {
            PersianCalendar pc = new PersianCalendar();
            label5.Text = pc.GetYear(DateTime.Now).ToString() + "/" + pc.GetMonth(DateTime.Now).ToString() + "/" + pc.GetDayOfMonth(DateTime.Now).ToString();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Database db = new Database();
                if (db.Login(textBox1.Text, textBox2.Text))
                {
                    Hide();
                    FormMain f = new FormMain();
                    f.ShowDialog();
                }
                else
                    MessageBox.Show("رمز عبور یا نام کاربری نامعتبر است");
            }
        }
    }
}
