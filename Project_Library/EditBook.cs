﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class EditBook : Form
    {
        public EditBook()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            SqlCommand cmd = new SqlCommand("select * from tbl_book where name like N'%" + textBox1.Text + "%'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand("delete from tbl_book where id_book = '" + id + "'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            MessageBox.Show("اطلاعات با موفقیت حذف گردید");
            Edit_book_Load(null, null);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tbl_book set name='" + textBox2.Text + "' , author='" + textBox3.Text + "' , publish = '" + textBox4.Text + "' , subject = '" + textBox5.Text + "' where id_book = '" + id + "'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            MessageBox.Show("اطلاعات با موفقیت ویرایش گردید");

            SqlConnection con1 = new SqlConnection(Database.ConnectionString);
            SqlCommand cmd1 = new SqlCommand("select * from tbl_book where name like '%" + textBox1.Text + "%'")
            {
                Connection = con1
            };
            DataSet ds1 = new DataSet();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            da1.Fill(ds1);
            dataGridView1.DataSource = ds1.Tables[0];
        }

        private void Edit_book_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'tbl_bookDataSet.tbl_book' table. You can move, or remove it, as needed.
            tbl_bookTableAdapter.Fill(tbl_bookDataSet.tbl_book);
        }
    }
}
