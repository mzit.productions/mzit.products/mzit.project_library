﻿using System.Data;
using System.Data.SqlClient;

namespace Project_Library
{
    internal class Database
    {
        /// public static string conStr = @"Data Source=.\SQLEXPRESS;Initial Catalog=Project_LibraryDB;User ID=sa;Password=sa_1234";
        public static string ConnectionString { get; set; } = @"Data Source=.\SQLEXPRESS;Initial Catalog=Project_LibraryDB;Integrated Security=True";

        private readonly SqlConnection con = new SqlConnection(Database.ConnectionString);

        public bool Login(string username, string password)
        {
            SqlCommand cmd = new SqlCommand("select * from tbl_user where username='" + username + "'and password='" + password + "'")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
                return false;
        }
        public DataSet Select_Person(string name)
        {
            SqlCommand cmd = new SqlCommand("select * from tbl_person where name like'%" + name + "%'")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;
        }

        public bool Insert_User(string user, string pass, string post)
        {
            SqlCommand cmd = new SqlCommand("insert into tbl_user(username,password,post) values(N'" + user + "','" + pass + "','" + post + "')")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return true;
        }

        public bool Insert_Person(string name, string family, string tell)
        {
            SqlCommand cmd = new SqlCommand("insert into tbl_person(name,family,tell) values(N'" + name + "',N'" + family + "','" + tell + "')")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return true;
        }
        public bool Insert_Book(string name, string author, string publish, string subject)
        {
            SqlCommand cmd = new SqlCommand("insert into tbl_book(name,author,publish,subject) values(N'" + name + "',N'" + author + "',N'" + publish + "','" + subject + "')")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return true;
        }
        public bool Insert_Amanat(string id_person, string id_book, string date_resive, bool delivery)
        {
            SqlCommand cmd = new SqlCommand("insert into tbl_trust(id_person,id_book,date_resive,delivery) values(N'" + id_person + "','" + id_book + "','" + date_resive + "','" + delivery + "')")
            {
                Connection = con
            };
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return true;
        }
        public DataSet Select_Amanat(string id_person)
        {
            SqlCommand cmd = new SqlCommand("select * from tbl_trust where id_person = '" + id_person + "'")
            {
                Connection = con
            };
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;
        }
    }
}







