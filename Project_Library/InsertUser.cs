﻿using System;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class InsertUser : Form
    {
        public InsertUser()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Database db = new Database();
            db.Insert_User(textBox1.Text, textBox2.Text, textBox3.Text);

            MessageBox.Show("ثبت کاربر با موفقیت انجام شد");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
