﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class SearchUser : Form
    {
        public SearchUser()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                SqlConnection con = new SqlConnection(Database.ConnectionString);
                SqlCommand cmd = new SqlCommand("select * from tbl_user where username like N'%" + textBox1.Text + "%'")
                {
                    Connection = con
                };
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.Columns[0].HeaderText = "نام کاربری";
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns[1].HeaderText = "پسورد";
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns[2].HeaderText = "پست";
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            else
                dataGridView1.DataSource = null;
        }

        private void SearchUser_Load(object sender, EventArgs e)
        {

        }
    }
}
