﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class SearchAmanat : Form
    {
        public SearchAmanat()
        {
            InitializeComponent();
        }

        private void SelectAmanat()
        {
            if (textBox1.Text.Length > 0)
            {
                Database db = new Database();
                DataSet ds = db.Select_Amanat(textBox1.Text);
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.Columns[0].HeaderText = "شناسه کاربری";
                dataGridView1.Columns[1].HeaderText = "شناسه کتاب";
                dataGridView1.Columns[2].HeaderText = "تاریخ دریافت کتاب";
                dataGridView1.Columns[3].HeaderText = "تحویل";
            }
            else
            { dataGridView1.DataSource = null; }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SelectAmanat();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SelectAmanat();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Database.ConnectionString);
            string id_person = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string id_book = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tbl_trust set delivery='" + true + "' where id_person = '" + id_person + "' and id_book = '" + id_book + "'")
            {
                Connection = con
            };
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            MessageBox.Show("اطلاعات با موفقیت ویرایش گردید");

            SelectAmanat();
        }
    }
}

