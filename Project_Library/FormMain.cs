﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void خروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "آیا مایل به خروح هستید؟",
                "خروج",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            { Application.Exit(); }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            PersianCalendar pc = new PersianCalendar();
            label1.Text = pc.GetHour(DateTime.Now).ToString() + ":" + pc.GetMinute(DateTime.Now).ToString() + ":" + pc.GetSecond(DateTime.Now).ToString();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            PersianCalendar pc = new PersianCalendar();
            label5.Text = pc.GetYear(DateTime.Now).ToString() + "/" + pc.GetMonth(DateTime.Now).ToString() + "/" + pc.GetDayOfMonth(DateTime.Now).ToString();
            wmp_1.URL = "1.mp3";
        }

        private void ثبتکاربرجدیدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertUser f = new InsertUser();
            f.ShowDialog();
        }

        private void حذفکاربرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteUser f = new DeleteUser();
            f.ShowDialog();
        }

        private void جستجوکاربرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchUser f = new SearchUser();
            f.ShowDialog();
        }

        private void ثبتعضویتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertPerson f = new InsertPerson();
            f.ShowDialog();
        }

        private void حذفوویرایشعضویتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditPerson f = new EditPerson();
            f.ShowDialog();
        }

        private void حذفعضویتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeletePerson f = new DeletePerson();
            f.ShowDialog();
        }

        private void جستجویاعضاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchPerson f = new SearchPerson();
            f.ShowDialog();
        }

        private void ثبتکتابToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertBook f = new InsertBook();
            f.ShowDialog();
        }

        private void حذفوویراشکتابToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditBook f = new EditBook();
            f.ShowDialog();
        }

        private void جستجویکتابToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchBook f = new SearchBook();
            f.ShowDialog();
        }

        private void ثبتاماناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertAmanat f = new InsertAmanat();
            f.ShowDialog();
        }

        private void کتابهایامانتدادهشدهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchAmanat f = new SearchAmanat();
            f.ShowDialog();
        }
    }
}
