﻿using System;
using System.Windows.Forms;

namespace Project_Library
{
    public partial class InsertAmanat : Form
    {
        public InsertAmanat()
        {
            InitializeComponent();
        }

        private void Insert_amont_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Database db = new Database();
            db.Insert_Amanat(textBox1.Text, textBox2.Text, textBox3.Text, false);

            MessageBox.Show("ثبت امانت با موفقیت انجام شد");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
